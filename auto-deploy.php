<?php

date_default_timezone_set('America/New_York');

/*
 * This is our Deploy class (do not edit)
 */

class Deploy {

  // A callback function to call after the deploy has finished.
  public $post_deploy;
  
  // The name of the file that will be used for logging deployments
  // Set to FALSE to disable logging
  private $_log = 'deployments.log';

  //The timestamp format used for logging
  private $_date_format = 'Y-m-d H:i:sP';

  // The name of the branch to pull from
  private $_branch = 'master';

  // The name of the remote to pull from
  private $_remote = 'origin';

  // Relative or absolute path to directory
  private $_directory;

  public function __construct($directory, $options = array())
  {
    $this->_directory = realpath($directory);

    $available_options = array('log', 'date_format', 'branch', 'remote');

    foreach ($options as $option => $value)
    {
      if (in_array($option, $available_options))
      {
        $this->{'_'.$option} = $value;
      }
    }
  }

  // Writes a message to the log file.
  public function log($message, $type = 'INFO')
  {
    if ($this->_log)
    {
      // Set the name of the log file
      $filename = $this->_log;

      if ( ! file_exists($filename))
      {
          // Create the log file
          file_put_contents($filename, '');
          chmod($filename, 0666);
      }

      // Write the message into the log file
      file_put_contents($filename, date($this->_date_format).' --- '.$type.': '.$message.PHP_EOL, FILE_APPEND);
    }
  }

  // Executes the necessary commands to deploy the website.
  public function execute() {
    try {
      // Make sure we're in the right directory
      chdir($this->_directory);
      
      // Add any untracked files and modifications
      exec('git add -A', $output);
      exec('git commit -a -m"I found changes and committed them"', $output);

      // Update the local repository
      exec('git pull -s ours'.$this->_remote.' '.$this->_branch, $output);
      
      // Push any changes that were found
      exec('git push '.$this->_remote.' '.$this->_branch, $output);

      // Secure the .git directory
      exec('chmod -R og-rx .git');
      
    } catch (Exception $e) {
        $this->log($e, 'ERROR');
    }
  }

}

/*
 * These are options that you can change
 */

$options = array(
  'log' => '/var/www/webozy.net/deploy.log',
  'date_format' => 'Y-m-d H:i:sP',
  'branch' => 'master',
  'remote' => 'origin',
);

$deploy = new Deploy('/var/www/webozy.net', $options);
$deploy->execute();